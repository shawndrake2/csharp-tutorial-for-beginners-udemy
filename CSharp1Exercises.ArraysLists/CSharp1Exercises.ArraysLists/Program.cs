﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp1Exercises.ArraysLists
{
    class Program
    {
        static void Main(string[] args)
        {
            var arrayLists = new ArraysLists();
            arrayLists.Exercise5();
        }
    }

    class ArraysLists
    {
        public void Exercise1()
        {
            var messages = new string[3] { "{0} likes your post;", "{0} and {1} like your post.", "{0}, {1}, and {2} others like your post." };
            var nameList = new List<string>();
            var input = "";
            do
            {
                Console.WriteLine("Enter a name: ");
                input = Console.ReadLine();
                if (input.Length > 0)
                {
                    nameList.Add(input);
                }
            } while (input.Length > 0);

            var count = nameList.Count;
            if (count > 0)
            {
                if (count == 1)
                {
                    Console.WriteLine(messages[0], nameList.ElementAt(0));
                } else if (count == 2)
                {
                    Console.WriteLine(messages[1], nameList.ElementAt(0), nameList.ElementAt(1));
                } else
                {
                    Console.WriteLine(messages[2], nameList.ElementAt(0), nameList.ElementAt(1), count - 2);
                }
            }
        }

        public void Exercise2()
        {
            Console.WriteLine("Enter your name: ");
            var input = Console.ReadLine();
            
            char[] inputArray = input.ToArray();

            var stringLength = inputArray.Length;
            for (int i = 0; i < stringLength; i++)
            {
                inputArray[i] = input[i];
            }
            Array.Reverse(inputArray);
            var reversedName = string.Join("", inputArray);
            Console.WriteLine("Your name reversed is {0}.", reversedName);
        }

        public void Exercise3()
        {
            var numberList = new List<int>();
            var unique = true;

            while (numberList.Count < 5)
            {
                if (unique)
                {
                    Console.WriteLine("Enter a number: ");
                }
                else
                {
                    Console.WriteLine("You already entered that number. Enter a different number: ");
                }
                var input = Console.ReadLine();
                var num = Convert.ToInt32(input);
                if (numberList.Contains(num))
                {
                    unique = false;
                    continue;
                }
                else
                {
                    unique = true;
                    numberList.Add(num);
                }
            }
            numberList.Sort();
            foreach(var number in numberList)
            {
                Console.WriteLine(number);
            }
        }

        public void Exercise4()
        {
            const string quit = "Quit";
            var numberList = new List<int>();
            var input = "";
            do
            {
                Console.WriteLine("Enter a number or '{0}' to exit: ", quit);
                input = Console.ReadLine();
                if (input != quit)
                {
                    numberList.Add(Convert.ToInt32(input));
                }
            } while (input != quit);

            // Print unique values
            foreach (var number in numberList.Distinct())
            {
                Console.WriteLine(number);
            }
        }

        public void Exercise5()
        {
            var valid = false;
            var numbers = new List<string>();

            Console.WriteLine("Enter a comma separated list of numbers: ");

            do
            {
                var input = Console.ReadLine();
                numbers = input.Split(',').ToList();

                if (String.IsNullOrEmpty(input) || numbers.Count < 5)
                {
                    Console.WriteLine("Invalid List. Please try again: ");
                }
                else
                {
                    valid = true;
                }
            } while (!valid);

            numbers.Sort();
            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine(numbers[i]);
            }
        }
    }
}
