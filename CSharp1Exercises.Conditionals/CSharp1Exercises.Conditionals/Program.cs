﻿using System;

namespace CSharp1Exercises.Conditionals
{
    class Program
    {
        static void Main(string[] args)
        {
            var test = new Conditionals();
            test.Exercise4();
        }
    }

    class Conditionals
    {
        public void Exercise1 ()
        {
            int number = getIntFromInput("Enter a number between 1 and 10: ");
            Console.WriteLine((number > 0 && number <= 10) ? "Valid" : "Invalid");
        }

        public void Exercise2 ()
        {
            Console.WriteLine("Please enter 2 numbers.....");
            int number1 = getIntFromInput("Enter number 1: ");
            int number2 = getIntFromInput("Enter number 2: ");
            Console.WriteLine("The largest number entered is {0}.", Math.Max(number1, number2));
        }

        public void Exercise3 ()
        {
            Console.WriteLine("Please enter your image dimensions.....");
            int width = getIntFromInput("Enter width: ");
            int length = getIntFromInput("Enter length: ");

            string orientation = width > length ? "landscape" : "portrait";
            Console.WriteLine("Your image's orientation is {0}.", orientation);
        }

        public void Exercise4 ()
        {
            int speedLimit = getIntFromInput("Please enter a speed limit: ");
            int carSpeed = getIntFromInput("Car speed: ");
            string message = "OK!"; // We can assume everyone is following the speed limit.... right? RIGHT????
            int demerits = 0; // Since everyone is going the speed limit, we can assume 0 demerits too

            bool isWithinSpeedLimit = carSpeed <= speedLimit;

            if (!isWithinSpeedLimit)
            {
                demerits = calculateDemerits(carSpeed,speedLimit);
                if (demerits == 0)
                {
                    message = "You got off with a warning this time. Please slow down.";
                } else
                {
                    message = demerits <= 12 ? "You've earned {0} demerits." : "You have over 12 demerits. Your license has been suspended.";
                }
            }
            Console.WriteLine(message, demerits);
        }

        public int calculateDemerits (int speed, int limit)
        {
            int overage = speed - limit;
            int demerits = overage / 5; // 1 demerit for every 5 mph over the limit
            return demerits;
        }

        public int getIntFromInput(string prompt)
        {
            Console.WriteLine(prompt);
            return Convert.ToInt32(Console.ReadLine());
        }
    }
}
