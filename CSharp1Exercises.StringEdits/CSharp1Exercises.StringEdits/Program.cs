﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp1Exercises.StringEdits
{
    class Program
    {
        static void Main(string[] args)
        {
            var stringManip = new StringManipulation();
            stringManip.Exercise3();
        }
    }

    class StringManipulation
    {
        public void Exercise1()
        {
            Console.WriteLine("Enter a set of numbers separated by a hyphen (Ex 2-5-7) : ");
            var input = Console.ReadLine();
            var consecutive = true;
            var numbers = input.Split('-');
            var lastNumber = -1;
            foreach (var number in numbers)
            {
               if (lastNumber == -1)
                {
                    lastNumber = Convert.ToInt32(number);
                    continue;
                }
               if ((lastNumber + 1) != Convert.ToInt32(number))
                {
                    consecutive = false;
                    break;
                }
                else
                {
                    lastNumber = Convert.ToInt32(number);
                }
            }
            Console.WriteLine(consecutive ? "Consecutive" : "Not Consecutive");
        }

        public void Exercise2()
        {
            Console.WriteLine("Enter a set of numbers separated by a hyphen (Ex 2-5-7) : ");
            var input = Console.ReadLine();

            if (!String.IsNullOrWhiteSpace(input))
            {
                var numbers = input.Split('-');
                foreach (var number in numbers)
                {
                    if (input.IndexOf(number) != input.LastIndexOf(number))
                    {
                        Console.WriteLine("Duplicate");
                        break;
                    }
                }
            }
        }

        public void Exercise3()
        {
            Console.WriteLine("Enter a time in 24-hour format (Ex 19:00) : ");
            var input = Console.ReadLine();

            var invalid = String.IsNullOrWhiteSpace(input);
            if (!invalid)
            { 
                var enteredTime = input.Split(':');
                invalid = enteredTime.Length != 2;
                if (!invalid)
                {
                    var hour = Convert.ToInt32(enteredTime[0]);
                    var minute = Convert.ToInt32(enteredTime[1]);
                    invalid = (hour < 0 || hour > 23) || (minute < 0 || minute > 59);
                }
            }

            Console.WriteLine( invalid ? "Invalid Time" : "OK" );
        }

        public static int convertToInt(string expression)
        {
            return Convert.ToInt32(expression);
        }

        public void Exercise4()
        {

        }

        public void Exercise5()
        {

        }
    }
}
