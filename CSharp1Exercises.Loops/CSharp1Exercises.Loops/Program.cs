﻿using System;

namespace CSharp1Exercises.Loops
{
    class Program
    {
        static void Main(string[] args)
        {
            Loops loopsExercises = new Loops();
            loopsExercises.Exercise5();
        }
    }

    class Loops
    {
        public void Exercise1()
        {
            int numbersDivisibleBy3 = 0;
            for(int i = 1; i <=100; i++)
            {
                if (i%3 == 0)
                {
                    numbersDivisibleBy3++;
                }
            }

            Console.WriteLine("There are {0} numbers between 1 and 100 that are divisible by 3.", numbersDivisibleBy3);
        }

        public void Exercise2()
        {
            string input = "";
            int sum = 0;
            while (input != "ok")
            {
                int output;

                Console.WriteLine("Please enter a number or 'ok' to exit: ");
                input = Console.ReadLine();

                if (int.TryParse(input, out output)){
                    sum += output;
                }
            }
            Console.WriteLine("The sum of all numbers entered is {0}.", sum);
        }

        public void Exercise3()
        {
            int number = 0;
            Console.WriteLine("Please enter a number: ");
            var input = Console.ReadLine();
            number = Convert.ToInt32(input);

            int factorial = number;
            for (int i = number-1; i >= 1; i--)
            {
                factorial *= i;
            }

            Console.WriteLine("{0}! = {1}", number, factorial);
        }

        public void Exercise4()
        {
            const int min = 1;
            const int max = 10;

            var random = new Random();
            int randomNumber = random.Next(min, max);

            int guess = 1;
            bool winner = false;
            do
            {
                Console.WriteLine("Guess a number between {0} and {1}: ", min, max);
                var input = Console.ReadLine();
                int number = Convert.ToInt32(input);

                if (number == randomNumber)
                {
                    winner = true;
                    break;
                }
                guess++;
            } while (guess <= 4);

            Console.WriteLine( winner ? "You won!" : "Sorry, you lost.");
        }

        public void Exercise5()
        {
            Console.WriteLine("Enter any combination of numbers separated by a comma (Ex 1,2,3): ");
            var input = Console.ReadLine();
            var numbers = input.Split(',');

            int maxEntered = Convert.ToInt32(numbers[0]);

            foreach (var str in numbers)
            {
                var number = Convert.ToInt32(str);
                if (number > maxEntered)
                {
                    maxEntered = number;
                }
            }
            Console.WriteLine("The maximum number entered was {0}.", maxEntered);
        }
    }
}
